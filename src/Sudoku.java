import java.io.*;
import java.util.*;

public class Sudoku {
    public static final int THREE = 3;
    public static final int THREE_SQ = THREE * THREE;
    public static final int ALL_ALLOWED = ~(~0 << THREE_SQ) << 1;

    private static final String FILE_CHARS = "x123456789";
    private static final String ROW_CHARS = "ABCDEFGHI";
    private static final String COL_CHARS = "123456789";
    private static final char SEP_CHAR = ':';
    private static final String[] LEVELS = new String[] {
        "levels/emerentius/000.txt",
        "levels/emerentius/001.txt",
        "levels/emerentius/002.txt",
        "levels/emerentius/003.txt",
        "levels/emerentius/004.txt",
        "levels/emerentius/005.txt",
        "levels/emerentius/006.txt",
        "levels/emerentius/007.txt",
        "levels/emerentius/008.txt",
        "levels/emerentius/009.txt",
        "levels/emerentius/010.txt",
        "levels/emerentius/011.txt",
        "levels/emerentius/012.txt",
        "levels/emerentius/013.txt",
        "levels/emerentius/014.txt",
        "levels/emerentius/015.txt",
        "levels/emerentius/016.txt",
        "levels/emerentius/017.txt",
        "levels/emerentius/018.txt",
        "levels/emerentius/019.txt",
        "levels/emerentius/020.txt",
        "levels/tdoku/000.txt",
        "levels/tdoku/001.txt",
        "levels/tdoku/002.txt",
        "levels/tdoku/003.txt",
        "levels/tdoku/004.txt",
        "levels/tdoku/005.txt",
        "levels/tdoku/006.txt",
        "levels/tdoku/007.txt",
        "levels/tdoku/008.txt",
        "levels/tdoku/009.txt",
        "levels/tdoku/010.txt",
        "levels/tdoku/011.txt",
        "levels/tdoku/012.txt",
        "levels/tdoku/013.txt",
        "levels/tdoku/014.txt",
        "levels/tdoku/015.txt",
        "levels/tdoku/016.txt",
        "levels/tdoku/017.txt",
    };

    private byte[][] grid;

    public Sudoku(Sudoku s) {
        this.grid = s.grid.clone();
    }

    public Sudoku() {
        this.grid = new byte[Sudoku.THREE_SQ][];
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            this.grid[i] = new byte[Sudoku.THREE_SQ];
        }
    }

    public static Sudoku fromFile(String file) throws FileNotFoundException, IOException {
        BufferedReader r = new BufferedReader(new FileReader(file));
        String line;
        Sudoku s = new Sudoku();

        int x = 0;
        while ((line = r.readLine()) != null) {
            if (line.length() == 0 || line.charAt(0) == '#') {
                continue;
            }

            String[] cells = line.split(" ");
            if (cells.length != Sudoku.THREE_SQ) {
                throw new IllegalArgumentException(file + ": bad line length at x=" + x);
            }

            for (int y = 0; y < Sudoku.THREE_SQ; y++) {
                String cell = cells[y];
                if (cell.length() != 1) {
                    throw new IllegalArgumentException(file + ": bad cell length at x=" + x + " y=" + y);
                }
                byte value = (byte)Sudoku.FILE_CHARS.indexOf(cell.charAt(0));

                if (value == -1) {
                    throw new IllegalArgumentException(file + ": bad value at x=" + x + " y=" + y);
                }

                if (!s.set(x, y, value)) {
                    throw new IllegalArgumentException(file + ": contradiction at x=" + x + " y=" + y);
                }
            }

            x++;
        }
        if (x != Sudoku.THREE_SQ) {
            throw new IllegalArgumentException(file + ": too few lines");
        }

        return s;
    }

    public int allowed(int x, int y) {
        int boxX = (x / this.THREE) * this.THREE;
        int boxY = (y / this.THREE) * this.THREE;

        int allowed = Sudoku.ALL_ALLOWED;
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            int boxOffsetX = i % this.THREE;
            int boxOffsetY = i / this.THREE;

            // if the cell is empty it has a value of 0
            byte rowCell = this.grid[i][y];
            byte colCell = this.grid[x][i];
            byte boxCell = this.grid[boxX + boxOffsetX][boxY + boxOffsetY];
            int rowMask = 1 << rowCell;
            int colMask = 1 << colCell;
            int boxMask = 1 << boxCell;
            allowed &= ~rowMask & ~colMask & ~boxMask;
        }
        return allowed;
    }

    public boolean isValid() {
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            int boxX = (i % this.THREE) * this.THREE;
            int boxY = (i / this.THREE) * this.THREE;

            int allowedInRow = Sudoku.ALL_ALLOWED;
            int allowedInCol = Sudoku.ALL_ALLOWED;
            int allowedInBox = Sudoku.ALL_ALLOWED;
            for (int j = 0; j < Sudoku.THREE_SQ; j++) {
                int boxOffsetX = j % this.THREE;
                int boxOffsetY = j / this.THREE;

                byte rowCell = this.grid[i][j];
                byte colCell = this.grid[j][i];
                byte boxCell = this.grid[boxX + boxOffsetX][boxY + boxOffsetY];
                int rowMask = 1 << rowCell;
                int colMask = 1 << colCell;
                int boxMask = 1 << boxCell;
                if (((allowedInRow & rowMask) == 0 && rowCell != 0)
                        || ((allowedInCol & colMask) == 0 && colCell != 0)
                        || ((allowedInBox & boxMask) == 0 && boxCell != 0)) {
                    return false;
                }
                allowedInRow &= ~rowMask;
                allowedInCol &= ~colMask;
                allowedInBox &= ~boxMask;
            }
        }
        return true;
    }

    public boolean isFull() {
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            for (int j = 0; j < Sudoku.THREE_SQ; j++) {
                if (this.grid[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public byte get(int x, int y) {
        return this.grid[x][y];
    }

    public boolean set(int x, int y, byte value) {
        if ((this.allowed(x, y) & (1 << value)) == 0 && value != 0) {
            return false;
        }
        this.grid[x][y] = value;
        return true;
    }

    public boolean solve() {
        if (this.isFull()) {
            return true;
        }

        int numAllowed = Sudoku.THREE_SQ + 1;
        int x = -1, y = -1;
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            for (int j = 0; j < Sudoku.THREE_SQ; j++) {
                int numAllowedHere = Integer.bitCount(this.allowed(i, j));
                if (this.get(i, j) == 0 && numAllowedHere < numAllowed) {
                    x = i;
                    y = j;
                    numAllowed = numAllowedHere;
                }
            }
        }

        if (numAllowed == 0) {
            return false;
        }

        int allowed = this.allowed(x, y);
        for (byte value = 1; value <= Sudoku.THREE_SQ; value++) {
            if ((allowed & (1 << value)) == 0) {
                continue;
            }

            this.set(x, y, value);
            if (this.solve()) {
                return true;
            }
        }
        this.set(x, y, (byte)0);
        return false;
    }

    private String valueColSep(int x, int y) {
        int n = (y % Sudoku.THREE == 0) ? 3 : 1;
        return "" + Box.tlbr(n, 0, n, 0);
    }

    private String valueCell(int x, int y) {
        return " " + " 123456789".charAt(this.grid[x][y]) + " ";
    }

    private String valueRow(int x) {
        String s = "";
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            s += this.valueColSep(x, i) + this.valueCell(x, i);
        }
        s += this.valueColSep(x, Sudoku.THREE_SQ) + '\n';
        return s;
    }

    private String sepRowColSep(int x, int y) {
        int n = (y % Sudoku.THREE == 0) ? 3 : 1;
        int t = (x == 0) ? 0 : n;
        int b = (x == Sudoku.THREE_SQ) ? 0 : n;

        int m = (x % Sudoku.THREE == 0) ? 3 : 1;
        int l = (y == 0) ? 0 : m;
        int r = (y == Sudoku.THREE_SQ) ? 0 : m;
        return "" + Box.tlbr(t, l, b, r);
    }

    private String sepRowCell(int x, int y) {
        int n = (x % Sudoku.THREE == 0) ? 3 : 1;
        return "" + Box.tlbr(0, n, 0, n) + Box.tlbr(0, n, 0, n) + Box.tlbr(0, n, 0, n);
    }

    private String sepRow(int x) {
        String s = "";
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            s += this.sepRowColSep(x, i) + this.sepRowCell(x, i);
        }
        s += this.sepRowColSep(x, Sudoku.THREE_SQ) + ((x == Sudoku.THREE_SQ) ? "" : "\n");
        return s;
    }

    public String toString() {
        String s = "";
        for (int i = 0; i < Sudoku.THREE_SQ; i++) {
            s += this.sepRow(i) + this.valueRow(i);
        }
        s += this.sepRow(Sudoku.THREE_SQ);
        return s;
    }

    private static String randomLevel() {
        Random rng = new Random();
        int i = (rng.nextInt() & Integer.MAX_VALUE) % Sudoku.LEVELS.length;
        return Sudoku.LEVELS[i];
    }

    private static void usage() {
        System.out.println("unknown format, use XY:Z");
        System.out.println("X: row A-I");
        System.out.println("Y: column 1-9");
        System.out.println("Z: value 1-9");
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        if (args.length == 0 || args[0].equals("perf")) {
            int n = 10;
            long startTotal = System.nanoTime();
            for (String level : Sudoku.LEVELS) {
                long start = System.nanoTime();
                for (int i = 0; i < n; i++) {
                    if(!Sudoku.fromFile(level).solve()) {
                        System.out.println("Failed " + level);
                        return;
                    }
                }
                long elapsed = System.nanoTime() - start;
                System.out.println(level + ": " + (float)elapsed / (n * 1.0e6) + " ms");
            }
            long elapsedTotal = System.nanoTime() - startTotal;
            System.out.println("TOTAL: " + (float)elapsedTotal / (n * 1.0e9) + " s");
        }

        if (args[0].equals("solve")) {
            String file = Sudoku.randomLevel();
            System.out.println(file);

            Sudoku s = Sudoku.fromFile(file);
            s.solve();
            System.out.println(s);
        }

        if (args[0].equals("interactive")) {
            String file = Sudoku.randomLevel();
            System.out.println(file);

            Sudoku solution = Sudoku.fromFile(file);
            solution.solve();

            Sudoku interactive = Sudoku.fromFile(file);
            System.out.println(interactive);

            Scanner scanner = new Scanner(System.in);
            while (!interactive.isFull()) {
                String line = scanner.nextLine();
                if (line.equals("solve")) {
                    break;
                }

                if (line.length() != 4 || line.charAt(2) != Sudoku.SEP_CHAR) {
                    Sudoku.usage();
                    continue;
                }

                char rowChar = line.charAt(0);
                char colChar = line.charAt(1);
                char valueChar = line.charAt(3);
                int row = Sudoku.ROW_CHARS.indexOf(rowChar);
                int col = Sudoku.COL_CHARS.indexOf(colChar);
                byte value = Byte.parseByte("" + valueChar);
                if (value < 1 || value > Sudoku.THREE_SQ || row == -1 || col == -1) {
                    Sudoku.usage();
                    continue;
                }

                if (solution.get(row, col) != value) {
                    System.out.println("wrong value");
                    continue;
                }
                interactive.set(row, col, value);
                System.out.println(interactive);
            }
        }

        if (args[0].equals("grid")) {
            System.out.println(new Sudoku());
        }
    }
}
