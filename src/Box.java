public class Box {
    public static final String CHARS = "NSDB";
    public static final char UNKNOWN = '?';

    // names are of the following scheme:
    // top left bottom right, each of which is either
    //   N for none
    //   S for single
    //   D for double
    //   B is for bold
    // so SSNN would be a single line bottom right corner
    //                        TLBR
    private static final char BBBB = '\u254B';
    private static final char BBBD = UNKNOWN;
    private static final char BBBN = '\u252B';
    private static final char BBBS = '\u2549';
    private static final char BBDB = UNKNOWN;
    private static final char BBDD = UNKNOWN;
    private static final char BBDN = UNKNOWN;
    private static final char BBDS = UNKNOWN;
    private static final char BBNB = '\u253B';
    private static final char BBND = UNKNOWN;
    private static final char BBNN = '\u251B';
    private static final char BBNS = '\u2539';
    private static final char BBSB = '\u2547';
    private static final char BBSD = UNKNOWN;
    private static final char BBSN = '\u2529';
    private static final char BBSS = '\u2543';
    private static final char BDBB = UNKNOWN;
    private static final char BDBD = UNKNOWN;
    private static final char BDBN = UNKNOWN;
    private static final char BDBS = UNKNOWN;
    private static final char BDDB = UNKNOWN;
    private static final char BDDD = UNKNOWN;
    private static final char BDDN = UNKNOWN;
    private static final char BDDS = UNKNOWN;
    private static final char BDNB = UNKNOWN;
    private static final char BDND = UNKNOWN;
    private static final char BDNN = UNKNOWN;
    private static final char BDNS = UNKNOWN;
    private static final char BDSB = UNKNOWN;
    private static final char BDSD = UNKNOWN;
    private static final char BDSN = UNKNOWN;
    private static final char BDSS = UNKNOWN;
    private static final char BNBB = '\u2523';
    private static final char BNBD = UNKNOWN;
    private static final char BNBN = '\u2503';  // continuous line
    //private static final char BNBN = '\u254F';  // dashed line (2)
    //private static final char BNBN = '\u2507';  // dashed line (3)
    //private static final char BNBN = '\u250B';  // dashed line (4)
    private static final char BNBS = '\u2520';
    private static final char BNDB = UNKNOWN;
    private static final char BNDD = UNKNOWN;
    private static final char BNDN = UNKNOWN;
    private static final char BNDS = UNKNOWN;
    private static final char BNNB = '\u2517';
    private static final char BNND = UNKNOWN;
    private static final char BNNN = '\u2579';
    private static final char BNNS = '\u2516';
    private static final char BNSB = '\u2521';
    private static final char BNSD = UNKNOWN;
    private static final char BNSN = '\u257F';
    private static final char BNSS = '\u251E';
    private static final char BSBB = '\u254A';
    private static final char BSBD = UNKNOWN;
    private static final char BSBN = '\u2528';
    private static final char BSBS = '\u2542';
    private static final char BSDB = UNKNOWN;
    private static final char BSDD = UNKNOWN;
    private static final char BSDN = UNKNOWN;
    private static final char BSDS = UNKNOWN;
    private static final char BSNB = '\u253A';
    private static final char BSND = UNKNOWN;
    private static final char BSNN = '\u251A';
    private static final char BSNS = '\u2538';
    private static final char BSSB = '\u2544';
    private static final char BSSD = UNKNOWN;
    private static final char BSSN = '\u2526';
    private static final char BSSS = '\u2540';
    private static final char DBBB = UNKNOWN;
    private static final char DBBD = UNKNOWN;
    private static final char DBBN = UNKNOWN;
    private static final char DBBS = UNKNOWN;
    private static final char DBDB = UNKNOWN;
    private static final char DBDD = UNKNOWN;
    private static final char DBDN = UNKNOWN;
    private static final char DBDS = UNKNOWN;
    private static final char DBNB = UNKNOWN;
    private static final char DBND = UNKNOWN;
    private static final char DBNN = UNKNOWN;
    private static final char DBNS = UNKNOWN;
    private static final char DBSB = UNKNOWN;
    private static final char DBSD = UNKNOWN;
    private static final char DBSN = UNKNOWN;
    private static final char DBSS = UNKNOWN;
    private static final char DDBB = UNKNOWN;
    private static final char DDBD = UNKNOWN;
    private static final char DDBN = UNKNOWN;
    private static final char DDBS = UNKNOWN;
    private static final char DDDB = UNKNOWN;
    private static final char DDDD = '\u256C';
    private static final char DDDN = '\u2563';
    private static final char DDDS = UNKNOWN;
    private static final char DDNB = UNKNOWN;
    private static final char DDND = '\u2569';
    private static final char DDNN = '\u255D';
    private static final char DDNS = UNKNOWN;
    private static final char DDSB = UNKNOWN;
    private static final char DDSD = UNKNOWN;
    private static final char DDSN = UNKNOWN;
    private static final char DDSS = UNKNOWN;
    private static final char DNBB = UNKNOWN;
    private static final char DNBD = UNKNOWN;
    private static final char DNBN = UNKNOWN;
    private static final char DNBS = UNKNOWN;
    private static final char DNDB = UNKNOWN;
    private static final char DNDD = '\u2560';
    private static final char DNDN = '\u2551';
    private static final char DNDS = '\u255F';
    private static final char DNNB = UNKNOWN;
    private static final char DNND = '\u255A';
    private static final char DNNN = UNKNOWN;
    private static final char DNNS = '\u2559';
    private static final char DNSB = UNKNOWN;
    private static final char DNSD = UNKNOWN;
    private static final char DNSN = UNKNOWN;
    private static final char DNSS = UNKNOWN;
    private static final char DSBB = UNKNOWN;
    private static final char DSBD = UNKNOWN;
    private static final char DSBN = UNKNOWN;
    private static final char DSBS = UNKNOWN;
    private static final char DSDB = UNKNOWN;
    private static final char DSDD = UNKNOWN;
    private static final char DSDN = '\u2562';
    private static final char DSDS = '\u256B';
    private static final char DSNB = UNKNOWN;
    private static final char DSND = UNKNOWN;
    private static final char DSNN = '\u255C';
    private static final char DSNS = '\u2568';
    private static final char DSSB = UNKNOWN;
    private static final char DSSD = UNKNOWN;
    private static final char DSSN = UNKNOWN;
    private static final char DSSS = UNKNOWN;
    private static final char NBBB = '\u2533';
    private static final char NBBD = UNKNOWN;
    private static final char NBBN = '\u2513';
    private static final char NBBS = '\u2531';
    private static final char NBDB = UNKNOWN;
    private static final char NBDD = UNKNOWN;
    private static final char NBDN = UNKNOWN;
    private static final char NBDS = UNKNOWN;
    private static final char NBNB = '\u2501';  // continuous line
    //private static final char NBNB = '\u254D';  // dashed line (2)
    //private static final char NBNB = '\u2505';  // dashed line (3)
    //private static final char NBNB = '\u2501';  // dashed line (4)
    private static final char NBND = UNKNOWN;
    private static final char NBNN = '\u2578';
    private static final char NBNS = '\u257E';
    private static final char NBSB = '\u252F';
    private static final char NBSD = UNKNOWN;
    private static final char NBSN = '\u2511';
    private static final char NBSS = '\u252D';
    private static final char NDBB = UNKNOWN;
    private static final char NDBD = UNKNOWN;
    private static final char NDBN = UNKNOWN;
    private static final char NDBS = UNKNOWN;
    private static final char NDDB = UNKNOWN;
    private static final char NDDD = '\u2566';
    private static final char NDDN = '\u2557';
    private static final char NDDS = UNKNOWN;
    private static final char NDNB = UNKNOWN;
    private static final char NDND = '\u2550';
    private static final char NDNN = UNKNOWN;
    private static final char NDNS = UNKNOWN;
    private static final char NDSB = UNKNOWN;
    private static final char NDSD = '\u2564';
    private static final char NDSN = '\u2555';
    private static final char NDSS = UNKNOWN;
    private static final char NNBB = '\u250F';
    private static final char NNBD = UNKNOWN;
    private static final char NNBN = '\u257B';
    private static final char NNBS = '\u250E';
    private static final char NNDB = UNKNOWN;
    private static final char NNDD = '\u2554';
    private static final char NNDN = UNKNOWN;
    private static final char NNDS = '\u2553';
    private static final char NNNB = '\u257A';
    private static final char NNND = UNKNOWN;
    private static final char NNNN = ' ';
    private static final char NNNS = '\u2576';
    private static final char NNSB = '\u250D';
    private static final char NNSD = '\u2552';
    private static final char NNSN = '\u2577';
    private static final char NNSS = '\u250C';  // sharp corner
    //private static final char NNSS = '\u256D';  // round corner
    private static final char NSBB = '\u2532';
    private static final char NSBD = UNKNOWN;
    private static final char NSBN = '\u2512';
    private static final char NSBS = '\u2530';
    private static final char NSDB = UNKNOWN;
    private static final char NSDD = UNKNOWN;
    private static final char NSDN = '\u2556';
    private static final char NSDS = '\u2565';
    private static final char NSNB = '\u257C';
    private static final char NSND = UNKNOWN;
    private static final char NSNN = '\u2574';
    private static final char NSNS = '\u2500';  // continuous line
    //private static final char NSNS = '\u254C';  // dashed line (2)
    //private static final char NSNS = '\u2504';  // dashed line (3)
    //private static final char NSNS = '\u2508';  // dashed line (4)
    private static final char NSSB = '\u252E';
    private static final char NSSD = UNKNOWN;
    private static final char NSSN = '\u2510';  // sharp corner
    //private static final char NSSN = '\u256E';  // round corner
    private static final char NSSS = '\u252C';
    private static final char SBBB = '\u2548';
    private static final char SBBD = UNKNOWN;
    private static final char SBBN = '\u252A';
    private static final char SBBS = '\u2545';
    private static final char SBDB = UNKNOWN;
    private static final char SBDD = UNKNOWN;
    private static final char SBDN = UNKNOWN;
    private static final char SBDS = UNKNOWN;
    private static final char SBNB = '\u2537';
    private static final char SBND = UNKNOWN;
    private static final char SBNN = '\u2519';
    private static final char SBNS = '\u2535';
    private static final char SBSB = '\u253F';
    private static final char SBSD = UNKNOWN;
    private static final char SBSN = '\u2525';
    private static final char SBSS = '\u253D';
    private static final char SDBB = UNKNOWN;
    private static final char SDBD = UNKNOWN;
    private static final char SDBN = UNKNOWN;
    private static final char SDBS = UNKNOWN;
    private static final char SDDB = UNKNOWN;
    private static final char SDDD = UNKNOWN;
    private static final char SDDN = UNKNOWN;
    private static final char SDDS = UNKNOWN;
    private static final char SDNB = UNKNOWN;
    private static final char SDND = '\u2567';
    private static final char SDNN = '\u255B';
    private static final char SDNS = UNKNOWN;
    private static final char SDSB = UNKNOWN;
    private static final char SDSD = '\u256A';
    private static final char SDSN = '\u2561';
    private static final char SDSS = UNKNOWN;
    private static final char SNBB = '\u2522';
    private static final char SNBD = UNKNOWN;
    private static final char SNBN = '\u257D';
    private static final char SNBS = '\u251F';
    private static final char SNDB = UNKNOWN;
    private static final char SNDD = UNKNOWN;
    private static final char SNDN = UNKNOWN;
    private static final char SNDS = UNKNOWN;
    private static final char SNNB = '\u2515';
    private static final char SNND = '\u2558';
    private static final char SNNN = '\u2575';
    private static final char SNNS = '\u2514';  // sharp corner
    //private static final char SNNS = '\u2570';  // round corner
    private static final char SNSB = '\u251D';
    private static final char SNSD = '\u255E';
    private static final char SNSN = '\u2502';  // continuous line
    //private static final char SNSN = '\u254E';  // dashed line (2)
    //private static final char SNSN = '\u2506';  // dashed line (3)
    //private static final char SNSN = '\u250A';  // dashed line (4)
    private static final char SNSS = '\u251C';
    private static final char SSBB = '\u2546';
    private static final char SSBD = UNKNOWN;
    private static final char SSBN = '\u2527';
    private static final char SSBS = '\u2541';
    private static final char SSDB = UNKNOWN;
    private static final char SSDD = UNKNOWN;
    private static final char SSDN = UNKNOWN;
    private static final char SSDS = UNKNOWN;
    private static final char SSNB = '\u2536';
    private static final char SSND = UNKNOWN;
    private static final char SSNN = '\u2518';  // sharp corner
    //private static final char SSNN = '\u256F';  // round corner
    private static final char SSNS = '\u2534';
    private static final char SSSB = '\u253E';
    private static final char SSSD = UNKNOWN;
    private static final char SSSN = '\u2524';
    private static final char SSSS = '\u253C';


    // generated with:
    // ```python3
    // s = "NSDB"
    // for a in s:
    //     print("{")
    //     for b in s:
    //         print("    {")
    //         for c in s:
    //             print("        { ", end="")
    //             for d in s:
    //                 print(a + b + c + d + ", ", end="")
    //             print("}, ")
    //         print("    },")
    //     print("}, ")
    // ```
    private static final char[][][][] LOOKUP = {
        {
            {
                { NNNN, NNNS, NNND, NNNB, },
                { NNSN, NNSS, NNSD, NNSB, },
                { NNDN, NNDS, NNDD, NNDB, },
                { NNBN, NNBS, NNBD, NNBB, },
            },
            {
                { NSNN, NSNS, NSND, NSNB, },
                { NSSN, NSSS, NSSD, NSSB, },
                { NSDN, NSDS, NSDD, NSDB, },
                { NSBN, NSBS, NSBD, NSBB, },
            },
            {
                { NDNN, NDNS, NDND, NDNB, },
                { NDSN, NDSS, NDSD, NDSB, },
                { NDDN, NDDS, NDDD, NDDB, },
                { NDBN, NDBS, NDBD, NDBB, },
            },
            {
                { NBNN, NBNS, NBND, NBNB, },
                { NBSN, NBSS, NBSD, NBSB, },
                { NBDN, NBDS, NBDD, NBDB, },
                { NBBN, NBBS, NBBD, NBBB, },
            },
        },
        {
            {
                { SNNN, SNNS, SNND, SNNB, },
                { SNSN, SNSS, SNSD, SNSB, },
                { SNDN, SNDS, SNDD, SNDB, },
                { SNBN, SNBS, SNBD, SNBB, },
            },
            {
                { SSNN, SSNS, SSND, SSNB, },
                { SSSN, SSSS, SSSD, SSSB, },
                { SSDN, SSDS, SSDD, SSDB, },
                { SSBN, SSBS, SSBD, SSBB, },
            },
            {
                { SDNN, SDNS, SDND, SDNB, },
                { SDSN, SDSS, SDSD, SDSB, },
                { SDDN, SDDS, SDDD, SDDB, },
                { SDBN, SDBS, SDBD, SDBB, },
            },
            {
                { SBNN, SBNS, SBND, SBNB, },
                { SBSN, SBSS, SBSD, SBSB, },
                { SBDN, SBDS, SBDD, SBDB, },
                { SBBN, SBBS, SBBD, SBBB, },
            },
        },
        {
            {
                { DNNN, DNNS, DNND, DNNB, },
                { DNSN, DNSS, DNSD, DNSB, },
                { DNDN, DNDS, DNDD, DNDB, },
                { DNBN, DNBS, DNBD, DNBB, },
            },
            {
                { DSNN, DSNS, DSND, DSNB, },
                { DSSN, DSSS, DSSD, DSSB, },
                { DSDN, DSDS, DSDD, DSDB, },
                { DSBN, DSBS, DSBD, DSBB, },
            },
            {
                { DDNN, DDNS, DDND, DDNB, },
                { DDSN, DDSS, DDSD, DDSB, },
                { DDDN, DDDS, DDDD, DDDB, },
                { DDBN, DDBS, DDBD, DDBB, },
            },
            {
                { DBNN, DBNS, DBND, DBNB, },
                { DBSN, DBSS, DBSD, DBSB, },
                { DBDN, DBDS, DBDD, DBDB, },
                { DBBN, DBBS, DBBD, DBBB, },
            },
        },
        {
            {
                { BNNN, BNNS, BNND, BNNB, },
                { BNSN, BNSS, BNSD, BNSB, },
                { BNDN, BNDS, BNDD, BNDB, },
                { BNBN, BNBS, BNBD, BNBB, },
            },
            {
                { BSNN, BSNS, BSND, BSNB, },
                { BSSN, BSSS, BSSD, BSSB, },
                { BSDN, BSDS, BSDD, BSDB, },
                { BSBN, BSBS, BSBD, BSBB, },
            },
            {
                { BDNN, BDNS, BDND, BDNB, },
                { BDSN, BDSS, BDSD, BDSB, },
                { BDDN, BDDS, BDDD, BDDB, },
                { BDBN, BDBS, BDBD, BDBB, },
            },
            {
                { BBNN, BBNS, BBND, BBNB, },
                { BBSN, BBSS, BBSD, BBSB, },
                { BBDN, BBDS, BBDD, BBDB, },
                { BBBN, BBBS, BBBD, BBBB, },
            },
        },
    };

    public static char tlbr(int top, int left, int bottom, int right) {
        return Box.LOOKUP[top][left][bottom][right];
    }

    private static int parseCharToIndex(char c) {
        if (c == ' ') {
            return 0;
        }

        int i = Box.CHARS.indexOf(c);
        if (i == -1) {
            throw new IllegalArgumentException("char \'" + c + "\' is not one of " + Box.CHARS);
        }
        return i;
    }

    public static char tlbr(String tlbr) {
        if (tlbr.length() != 4) {
            throw new IllegalArgumentException("string must contain exactly 4 characters for [top left bottom right]");
        }

        int top = Box.parseCharToIndex(tlbr.charAt(0));
        int left = Box.parseCharToIndex(tlbr.charAt(1));
        int bottom = Box.parseCharToIndex(tlbr.charAt(2));
        int right = Box.parseCharToIndex(tlbr.charAt(3));
        return Box.LOOKUP[top][left][bottom][right];
    }

    private static String[] draw(String[] inStrs) {
        int length = 0;
        for (String str : inStrs) {
            length = (str.length() > length) ? str.length() : length;
        }

        char[][] in = new char[inStrs.length][length];
        for (int i = 0; i < in.length; i++) {
            int j = 0;
            for (; j < inStrs[i].length(); j++) {
                in[i][j] = inStrs[i].charAt(j);
            }
            for (; j < in[i].length; j++) {
                in[i][j] = ' ';
            }
        }

        char[][] out = new char[(inStrs.length + 1) / 2][(length + 1) / 2];
        for (int i = 0; i < out.length; i++) {
            for (int j = 0; j < out[i].length; j++) {
                int y = i * 2;
                int x = j * 2;
                int t = y - 1;
                int l = x - 1;
                int b = y + 1;
                int r = x + 1;

                if (in[y][x] != '#') {
                    out[i][j] = in[y][x];
                } else {
                    char tc = (0 <= t && t < in.length) ? in[t][x] : ' ';
                    char lc = (0 <= l && l < in[y].length) ? in[y][l] : ' ';
                    char bc = (0 <= b && b < in.length) ? in[b][x] : ' ';
                    char rc = (0 <= r && r < in[y].length) ? in[y][r] : ' ';
                    int top = Box.parseCharToIndex(tc);
                    int left = Box.parseCharToIndex(lc);
                    int bottom = Box.parseCharToIndex(bc);
                    int right = Box.parseCharToIndex(rc);
                    out[i][j] = Box.LOOKUP[top][left][bottom][right];
                }
            }
        }

        String[] outStrs = new String[out.length];
        for (int i = 0; i < out.length; i++) {
            outStrs[i] = String.valueOf(out[i]);
        }
        return outStrs;
    }


    private static String[] box(String outerInner) {
        char o = outerInner.charAt(0);
        char i = outerInner.charAt(1);
        char n = 'N';
        return new String[] {
            "" + Box.tlbr("" + n + n + o + o) + Box.tlbr("" + n + o + i + o) + Box.tlbr("" + n + o + o + n),
            "" + Box.tlbr("" + o + n + o + i) + Box.tlbr("" + i + i + i + i) + Box.tlbr("" + o + i + o + n),
            "" + Box.tlbr("" + o + n + n + o) + Box.tlbr("" + i + o + n + o) + Box.tlbr("" + o + o + n + n),
        };
    }

    public static void main(String[] args) {
        System.out.print("   ");
        for (int j = 0; j < Box.CHARS.length(); j++) {
            char cj = Box.CHARS.charAt(j);
            System.out.print(" " + cj + " " + "  ");
        }
        System.out.println();

        for (int i = 0; i < Box.CHARS.length(); i++) {
            char ci = Box.CHARS.charAt(i);

            String[][] boxes = new String[Box.CHARS.length()][];
            for (int j = 0; j < Box.CHARS.length(); j++) {
                char cj = Box.CHARS.charAt(j);
                boxes[j] = Box.box("" + ci + cj);
            }

            for (int k = 0; k < boxes[0].length; k++) {
                System.out.print(" " + ((k == boxes[0].length / 2) ? ci : ' ') + " ");
                for (int j = 0; j < Box.CHARS.length(); j++) {
                    System.out.print(boxes[j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }

        String[] strs = Box.draw(new String[] {
            "#D#D#D#D#D#D#D# # # # # #",
            "D             S S        ",
            "# #B#B#B#B#B#B# # # # # #",
            "D B           S S        ",
            "# # #D#D#D#D#D# #S#S#S#S#",
            "D B D S       S S B S D S",
            "# # # # #S#B#S# #B#B#S#S#",
            "D B D S B  S  S S B S D S",
            "#S#S#S#S#S#S#S# #S#S#S#S#",
        });
        for (String s : strs) {
            System.out.println(s);
        }
    }
}

